### How to set this up

First, clone the repo.

    git clone https://bitbucket.org/jhenahan/laravel-example.git laravel

Then set up a virtual host pointing to laravel/public.

Note: Laravel requires PHP >= 5.3.7 with the mcrypt extension. This means you'll
also need libmcrypt >= 2.5.6 on your machine. It's available through any major
package manager (`brew`, `apt-get`, etc.).

Now, get composer

    cd laravel
    curl -sS https://getcomposer.org/installer | php
    chmod -R 777 app/storage

Run

    php composer.phar install

and wait for the dependencies to download.

Edit `bootstrap.php` to change `localhost` to your machine name, if necessary.

The application is configured to use a MySQL database called `laravel`.
Database configuration is found in `app/config/database.php`. Make sure to
change `host`, `username`, and `password` as appropriate. All you need to do is
create a database called `laravel` and the following steps will set up the rest
for you.

    php artisan migrate
    php artisan db:seed

And now you're ready. Navigate to your virtual host and try logging in and
changing your password or email.

The default user details are

    Username: TestUser (case-insensitive)
    Password: test

You can also register your own username if you prefer.

## Laravel PHP Framework

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)