<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete(); // Drop the old table if it exists.

        User::create(array(
                          'username' => 'TestUser',
                          'email'    => 'test@test.com',
                          'password' => Hash::make('test')
                     ));
    }
}