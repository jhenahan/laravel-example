<?php

class AccountController extends AuthController
{
    /**
     * First, we set a whitelist for the methods a guest can visit
     *
     * @access protected
     * @var array
     */
    protected $whitelist = array(
        'getLogin',
        'postLogin',
        'getRegister',
        'postRegister'
    );

    /**
     * Return the main user page
     *
     * @access public
     * @return View
     */

    public function getIndex()
    {
        return View::make('account/index')
               ->with('user', Auth::user());
    }

    public function postIndex()
    {
        // Form validation rules
        $rules = array(
            'email' => 'Required|Email'
        );

        // Check for password update
        if (Input::get('password'))
        {
            // Add password validation rules
            $rules['password']              = 'Required|Confirmed';
            $rules['password_confirmation'] = 'Required';
        }

        // Grab all our inputs
        $inputs = Input::all();

        $validator = Validator::make($inputs, $rules);

        // Check validation
        if ($validator->passes())
        {
            // Update user
            $user        = User::find(Auth::user()->id);
            $user->email = Input::get('email');

            if (Input::get('password') !== '')
            {
                $user->password = Hash::make(Input::get('password'));
            }

            $user->save(); // Save the updated info to the DB

            // Redirect to registration
            return Redirect::to('account')
                   ->with('success', 'Account updated!');
        }

        // If something goes wrong
        return Redirect::to('account')->withInput($inputs)
               ->withErrors($validator->getMessageBag());
    }

    /**
     * Return the login form
     *
     * @access public
     * @return View
     */

    public function getLogin()
    {
        // Already logged in?
        if (Auth::check())
        {
            return Redirect::to('account');
        }

        return View::make('account/login');
    }

    /**
     * Process the login form
     *
     * @access public
     * @return Redirect
     */

    public function postLogin()
    {
        // Validation rules
        $rules = array(
            'username' => 'Required',
            'password' => 'Required'
        );

        // Get inputs
        $username = Input::get('username');
        $password = Input::get('password');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            if (Auth::attempt(array(
                                   'username' => $username,
                                   'password' => $password
                              ))
            )
            {
                // Redirect to userpage
                return Redirect::to('account')
                       ->with('success', 'Log in successful!');
            }
            else
            {
                // Redirect to login
                return Redirect::to('account/login')
                       ->with('error', 'Email/password invalid.');
            }
        }

        // If something goes wrong
        return Redirect::to('account/login')
               ->withErrors($validator->getMessageBag());
    }

    /**
     * Account creation form
     *
     * @access public
     * @return View
     */

    public function getRegister()
    {
        // Check for login
        if (Auth::check())
        {
            return Redirect::to('account');
        }

        // Show the page
        return View::make('account/register');
    }

    /**
     * Process account creation.
     *
     * @access public
     * @return Redirect
     */

    public function postRegister()
    {
        // Validation rules
        $rules = array(
            'username'              => 'Required|Unique:users',
            'email'                 => 'Required|Email|Unique:users',
            'password'              => 'Required|Confirmed',
            'password_confirmation' => 'Required'
        );

        // Get inputs
        $inputs = Input::all();

        // Validate
        $validator = Validator::make($inputs, $rules);

        if ($validator->passes())
        {
            // Create the user
            $user           = new User;
            $user->username = Input::get('username');
            $user->email    = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->save();

            // Redirect to registration
            return Redirect::to('account/login')
                   ->with('success', 'Account created!');
        }

        // If something goes wrong
        return Redirect::to('account/register')->withInput($inputs)
               ->withErrors($validator->getMessageBag());
    }

    /**
     * Logout.
     *
     * @access public
     * @return Redirect
     */

    public function getLogout()
    {
        Auth::logout();

        return Redirect::to('account/login')->with('success', 'Logged out.');
    }
}