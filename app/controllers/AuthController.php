<?php


class AuthController extends BaseController
{

    protected $whitelist = array();

    /**
     * Initializes a new AuthController
     *
     * @access public
     * @return \AuthController
     */

    public function __construct()
    {
        parent::__construct();
        $this->beforeFilter('auth', array('except' => $this->whitelist));
    }
}